//
//  CalculatorBrain.swift
//  BaseUI
//
//  Created by Triicky on 14/02/2017.
//  Copyright © 2017 Triicky. All rights reserved.
//

import Foundation

class CalculatorBrain {
    
    init() {
        
    }
     
    static func calcul(calculOperator: String, firstNumber: String, secondNumber: String) -> Float {
        var result: Float
        
        switch calculOperator {
        case "+":
            result = firstNumber.floatValue + secondNumber.floatValue
        case "-":
            result = firstNumber.floatValue - secondNumber.floatValue
        case "÷":
            result = firstNumber.floatValue / secondNumber.floatValue
        case "x":
            result = firstNumber.floatValue * secondNumber.floatValue
        default:
            result = 0
        }
        
        return result
    }
    
    static func calculSpec(calculOperator: String, number: String) -> Float {
        var result: Float
        
        switch calculOperator {
        case "%":
            result = number.floatValue / 100
        case "+/-":
            result = number.floatValue * -1
        default:
            result = 0
        }
        
        return result
    }
}
