//
//  StringExtension.swift
//  BaseUI
//
//  Created by Triicky on 14/02/2017.
//  Copyright © 2017 Triicky. All rights reserved.
//

import Foundation

extension String {
    var floatValue: Float {
        return (self as NSString).floatValue
    }
}
