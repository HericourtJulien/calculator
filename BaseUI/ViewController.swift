//
//  ViewController.swift
//  BaseUI
//
//  Created by Triicky on 13/02/2017.
//  Copyright © 2017 Triicky. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var displayResult: UILabel!
    var initZero: Bool = true
    var isComma: Bool = false
    var isFinal: Bool = false
    var firstNumber: String = ""
    var calculOperator: String = ""
    
    @IBAction func numberTapped(_ sender: UIButton) {
        let number: String = sender.currentTitle!
        
        if isFinal {
            resetTapped(sender)
        }
        isFinal = false
        
        if initZero {
            displayResult.text = ""
            initZero =  false
        }
        
        displayResult.text = displayResult.text! + number
    }
    
    @IBAction func commaTapped(_ sender: Any) {
        if !isComma && !isFinal {
            displayResult.text = displayResult.text! + "."
            isComma = true
        }
        
        if initZero {
            initZero = false
        }
    }
    
    @IBAction func resetTapped(_ sender: Any) {
        displayResult.text = "0"
        initZero = true
        isComma = false
        isFinal = false
    }
    
    @IBAction func calculationTapped(_ sender: UIButton) {
        calculOperator = sender.currentTitle!
        
        if firstNumber == "" {
            firstNumber = displayResult.text!
        } else {
            let resultToDisplay = CalculatorBrain.calcul(calculOperator: calculOperator, firstNumber: firstNumber, secondNumber: displayResult.text!)
            
            displayResult.text = "\(resultToDisplay.clean)"
            firstNumber = displayResult.text!
        }
        
        self.resetTapped(sender)
    }

    @IBAction func calculationSecTapped(_ sender: UIButton) {
        calculOperator = sender.currentTitle!
        
        let resultToDisplay = CalculatorBrain.calculSpec(calculOperator: calculOperator, number: displayResult.text!)
        
        displayResult.text = "\(resultToDisplay.clean)"
    }
    

    @IBAction func equalsTapped(_ sender: Any) {
        guard firstNumber != "" else {
            return
        }
        
        isFinal = true
        
        let resultToDisplay = CalculatorBrain.calcul(calculOperator: calculOperator, firstNumber: firstNumber, secondNumber: displayResult.text!)
        
        displayResult.text = "\(resultToDisplay.clean)"
        firstNumber = ""
        calculOperator = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

