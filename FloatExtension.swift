//
//  FloatExtension.swift
//  BaseUI
//
//  Created by Triicky on 14/02/2017.
//  Copyright © 2017 Triicky. All rights reserved.
//

import Foundation

extension Float {
    var clean: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
